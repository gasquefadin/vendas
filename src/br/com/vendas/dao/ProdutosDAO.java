/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.vendas.dao;

import br.com.vendas.jdbc.ConnectionFactory;
import br.com.vendas.model.Clientes;
import br.com.vendas.model.Produtos;
import br.com.vendas.model.WebServiceCep;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author louis
 */
public class ProdutosDAO {
    // devera conter os metodos que farao integracao com a classe dos clientes
    //para cada tabela do BS, havera uma classe DAO

    private Connection con;

    public ProdutosDAO() { // este construtor roda toda vez que chamamos ClientesDAO cli = new ClientesDAO();
        this.con = new ConnectionFactory().getConnection();
    }

    public void cadastrarProduto(Produtos obj) {

        try {

            // 1 passo, criar o comando sql
            String sql = "insert into tb_produtos(descricao,preco,qtd_estoque,for_id)"
                    + " values (?,?,?,?)";

            // 2 passo - conectar o bD e organizar o comando sql
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, obj.getDescricao());
            stmt.setDouble(2, obj.getPreco());
            stmt.setInt(3, obj.getQtd_estoque());
            stmt.setInt(4, obj.getFornecedor().getId());


            // 3 passo - executar o comando sql
            stmt.execute();
            stmt.close();

            JOptionPane.showMessageDialog(null, "Cadastrado com sucesso !");

        } catch (SQLException erro) {

            JOptionPane.showMessageDialog(null, "Erro:" + erro);
        }

    }

    public void alterarProduto(Produtos obj) {

        try {

            // 1 passo, criar o comando sql
            String sql = "update tb_produtos set descricao=?,preco=?,qtd_estoque=?,for_id=?"
                    + " where id=?";

            // 2 passo - conectar o bD e organizar o comando sql
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, obj.getDescricao());
            stmt.setDouble(2, obj.getPreco());
            stmt.setInt(3, obj.getQtd_estoque());
            stmt.setObject(4, obj.getFornecedor().getNome());
            

            // 3 passo - executar o comando sql
            stmt.execute();
            stmt.close();

            JOptionPane.showMessageDialog(null, "Alterado com sucesso !");

        } catch (SQLException erro) {

            JOptionPane.showMessageDialog(null, "Erro:" + erro);
        }

    }

    public void excluirCliente(Clientes obj) {

        try {

            // 1 passo, criar o comando sql
            String sql = "delete from tb_clientes where id = ?";

            // 2 passo - conectar o bD e organizar o comando sql
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, obj.getId());

            // 3 passo - executar o comando sql
            stmt.execute();
            stmt.close();

            JOptionPane.showMessageDialog(null, "Excluído com sucesso !");

        } catch (SQLException erro) {

            JOptionPane.showMessageDialog(null, "Erro:" + erro);
        }

    }

    public List<Clientes> listarClientes() {

        try {

            // 1 passo - criar a lista 
            List<Clientes> lista = new ArrayList<>();

            // 2 passo criar sql
            String sql = "select * from tb_clientes";

            // 3 passo organizar e executar o comando
            PreparedStatement stmt = con.prepareStatement(sql);

            ResultSet rs = stmt.executeQuery(); // pega os resultados em lista

            while (rs.next()) {
                Clientes obj = new Clientes();
                obj.setId(rs.getInt("id"));
                obj.setNome(rs.getString("nome"));
                obj.setRg(rs.getString("rg"));
                obj.setCpf(rs.getString("cpf"));
                obj.setEmail(rs.getString("email"));
                obj.setTelefone(rs.getString("telefone"));
                obj.setCelular(rs.getString("celular"));
                obj.setCep(rs.getString("cep"));
                obj.setEndereco(rs.getString("endereco"));
                obj.setNumero(rs.getInt("numero"));
                obj.setComplemento(rs.getString("complemento"));
                obj.setBairro(rs.getString("bairro"));
                obj.setCidade(rs.getString("cidade"));
                obj.setEstado(rs.getString("estado"));

                lista.add(obj);
            }

            return lista;
        } catch (Exception erro) {
            JOptionPane.showMessageDialog(null, "Erro:" + erro);
            return null;
        }

    }

    // metodo consulta clientes por nome 
    public Clientes consultaNome(String nome) {

        try {

            String sql = "select * from tb_clientes where nome=?";

            // 3 passo organizar e executar o comando
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, nome);
            
            ResultSet rs = stmt.executeQuery(); // pega os resultados em lista
            Clientes obj = new Clientes();
            
             if (rs.next()) {
              
                obj.setId(rs.getInt("id"));
                obj.setNome(rs.getString("nome"));
                obj.setRg(rs.getString("rg"));
                obj.setCpf(rs.getString("cpf"));
                obj.setEmail(rs.getString("email"));
                obj.setTelefone(rs.getString("telefone"));
                obj.setCelular(rs.getString("celular"));
                obj.setCep(rs.getString("cep"));
                obj.setEndereco(rs.getString("endereco"));
                obj.setNumero(rs.getInt("numero"));
                obj.setComplemento(rs.getString("complemento"));
                obj.setBairro(rs.getString("bairro"));
                obj.setCidade(rs.getString("cidade"));
                obj.setEstado(rs.getString("estado"));
            }
             
             return obj;


        } catch (Exception erro) {
            JOptionPane.showMessageDialog(null, "Cliente não encontrado"+erro);
            return null;
        }
    }

    public List<Clientes> buscarClienteNome(String nome) {

        try {

            // 1 passo - criar a lista 
            List<Clientes> lista = new ArrayList<>();

            // 2 passo criar sql
            String sql = "select * from tb_clientes where nome like ?";

            // 3 passo organizar e executar o comando
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, nome);

            ResultSet rs = stmt.executeQuery(); // pega os resultados em lista

            while (rs.next()) {
                Clientes obj = new Clientes();
                obj.setId(rs.getInt("id"));
                obj.setNome(rs.getString("nome"));
                obj.setRg(rs.getString("rg"));
                obj.setCpf(rs.getString("cpf"));
                obj.setEmail(rs.getString("email"));
                obj.setTelefone(rs.getString("telefone"));
                obj.setCelular(rs.getString("celular"));
                obj.setCep(rs.getString("cep"));
                obj.setEndereco(rs.getString("endereco"));
                obj.setNumero(rs.getInt("numero"));
                obj.setComplemento(rs.getString("complemento"));
                obj.setBairro(rs.getString("bairro"));
                obj.setCidade(rs.getString("cidade"));
                obj.setEstado(rs.getString("estado"));

                lista.add(obj);
            }

            return lista;
        } catch (Exception erro) {
            JOptionPane.showMessageDialog(null, "Erro:" + erro);
            return null;
        }

    }
    
    	public Clientes buscaCep(String cep) {
       
            WebServiceCep webServiceCep = WebServiceCep.searchCep(cep);
       
            Clientes obj = new Clientes();

        if (webServiceCep.wasSuccessful()) {
            obj.setEndereco(webServiceCep.getLogradouroFull());
            obj.setCidade(webServiceCep.getCidade());
            obj.setBairro(webServiceCep.getBairro());
            obj.setEstado(webServiceCep.getUf());
            return obj;
        } else {
            JOptionPane.showMessageDialog(null, "Erro numero: " + webServiceCep.getResulCode());
            JOptionPane.showMessageDialog(null, "Descrição do erro: " + webServiceCep.getResultText());
            return null;
        }

    }
}
